[2022-12-15 18:57:11.762354 UTC] Starting env pool
[2022-12-15 18:57:11.955687 UTC] Starting iteration 0
[2022-12-15 18:57:11.957592 UTC] Start collecting samples
[2022-12-15 18:57:21.595355 UTC] Computing input variables for policy optimization
[2022-12-15 18:57:22.761222 UTC] Performing policy update
[2022-12-15 18:57:22.766636 UTC] Computing gradient in Euclidean space
[2022-12-15 18:57:24.327120 UTC] Computing approximate natural gradient using conjugate gradient algorithm
[2022-12-15 18:57:42.897441 UTC] Performing line search
[2022-12-15 18:57:43.739511 UTC] Updating baseline
[2022-12-15 18:58:11.841479 UTC] Computing logging information
-------------------------------------
| Iteration            | 0          |
| ExpectedImprovement  | 0.006056   |
| ActualImprovement    | 0.0047594  |
| ImprovementRatio     | 0.78589    |
| MeanKL               | 0.0084095  |
| Entropy              | 1.4189     |
| Perplexity           | 4.1327     |
| AveragePolicyStd     | 1          |
| AveragePolicyStd[0]  | 1          |
| AverageReturn        | -1125.4    |
| MinReturn            | -1816      |
| MaxReturn            | -842.99    |
| StdReturn            | 189.13     |
| AverageEpisodeLength | 200        |
| MinEpisodeLength     | 200        |
| MaxEpisodeLength     | 200        |
| StdEpisodeLength     | 0          |
| TotalNEpisodes       | 48         |
| TotalNSamples        | 9600       |
| ExplainedVariance    | -0.0010832 |
-------------------------------------
[2022-12-15 18:58:13.041752 UTC] Saving snapshot
[2022-12-15 18:58:13.078069 UTC] Starting iteration 1
[2022-12-15 18:58:13.079215 UTC] Start collecting samples
[2022-12-15 18:58:22.447507 UTC] Computing input variables for policy optimization
[2022-12-15 18:58:23.488623 UTC] Performing policy update
[2022-12-15 18:58:23.492212 UTC] Computing gradient in Euclidean space
[2022-12-15 18:58:24.992708 UTC] Computing approximate natural gradient using conjugate gradient algorithm
